function [ y ] = interpolate2d( x, x1, x2, y1, y2 )
% Simple 2D Interpolation
    y = y1 + (y2 - y1) .* (x - x1) ./ (x2 - x1);
end


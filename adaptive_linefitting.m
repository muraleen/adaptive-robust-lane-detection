% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

function [ beta, score, len ] = adaptive_linefitting( X, Y, Nmin )

    if nargin < 3
        Nmin = 3;
    end

    min_improvement_factor = 0.8; % MSE must be improved by this factor, else the adaption will be cancelled
    n_importance = 200; % The importance weight / N will be added to MSE

    % Make X and Y column vectors
    if size(X,2) > size(X,1)
        X = X';
    end
    if size(Y,2) > size(Y,1)
        Y = Y';
    end

    N = size(X,1);
    
    if N > Nmin
    
        X = [X ones(N,1)];

        score = 99999; % Low score is better
        beta_best = [];

        % STEP 1: Fit the data using linear regression
        beta = X \ Y;

        mse = 0;
        % Compute MSE
        for l = 1:N
            mse = mse + (X(l,:) * beta - Y(l))^2;
        end

        score = (mse + n_importance) / N;

        % Perform fitting for different N values (try to remove outliers)
        for n = N:-1:Nmin % Keep atleast 3!        
            % STEP 2: Compute mean square error (MSE)
            mse_best = 99999;
            k_mse_best = 0;
            % Compute MSE by omitting each element
            for k = 1:n
                % Perform regression without the element
                tmpX = [];
                tmpY = [];
                for l = 1:n
                    if l ~= k
                        tmpX = [tmpX; X(l,:)];
                        tmpY = [tmpY; Y(l)];
                    end
                end
                tmpBeta = tmpX \ tmpY;

                % Compute MSE without the element
                mse = 0;
                for l = 1:n-1
                    mse = mse + (tmpX(l,:) * tmpBeta - tmpY(l))^2;
                end

                mse = mse / n;

                if mse < mse_best
                    mse_best = mse;
                    k_mse_best = k;
                end
            end

            this_score = mse_best + n_importance / (n-1);

            if this_score / score >= min_improvement_factor
                % Improvement not sufficient: use beta_best
                break
            end

            % else, continue...
            % STEP 3: Remove outlying data point
            tmpX = [];
            tmpY = [];
            for k = 1:n
                if k ~= k_mse_best
                    tmpX = [tmpX; X(k,:)];
                    tmpY = [tmpY; Y(k)];
                end
            end
            X = tmpX;
            Y = tmpY;
            beta = X \ Y;

            score = this_score;
            beta_best = beta;
        end

        len = size(X,1);

    else
        score = 9999;
        beta = [];
        len = N;
    end
        
end


function [ out ] = saturate( in, lo, hi )
    out = in;
    for k = 1:length(in)
        if in(k) < lo(k)
            out(k) = lo(k);
        elseif in(k) > hi(k)
            out(k) = hi(k);
        end
    end
end


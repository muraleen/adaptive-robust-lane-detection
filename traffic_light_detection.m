% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

img = imread('data/deeplearn/examples/lights/red.jpg');

h = size(img,1);
w = size(img,2);

% Image
img = imcrop(img, [w/4 1 w/2 h/2]);
img = imresize(img, [h/4 w/4]);
img = im2bw(img);
img = 2.*img -1;
% figure;
% imshow(img);

h = size(img,1);
w = size(img,2);

% Template/Feature (5x5)
tpl1 = [-1 -1 -1 -1 -1;...
        -1 -1  1 -1 -1;...
        -1  1  1  1 -1;...
        -1 -1  1 -1 -1;...
        -1 -1 -1 -1 -1];

tpl2 = [-1 -1 -1 -1 -1;...
        -1 -1 -1 -1 -1;...
        -1 -1 -1 -1 -1;...
        -1 -1 -1 -1 -1;...
        -1 -1 -1 -1 -1];
       
% figure;
% imshow(tpl_red);

% Convolution (5x5)
c1 = conv2(img, tpl1)./25;
c2 = conv2(img, tpl2)./25;

d1 = c1;
d1(c1<0) = 0;
figure;
imshow(d1);

d2 = c2;
c2(c2<0) = 0;
figure;
imshow(d2);

% Max pooling (3x3)
% p = 3;
% e = zeros(ceil(h/p), ceil(w/p));
% for y = 1:ceil(h/p)
%     for x = 1:ceil(w/p)
%         e(y,x) = max(max(d((y-1)*p+1:y*p, (x-1)*p+1:x*p)));
%     end
% end
% figure;
% imshow(e);
%     
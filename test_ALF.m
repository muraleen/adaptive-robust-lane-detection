% Simple test for adaptive line-fitting (ALF) function
load test_ALF.mat

y_scanl = [];
lane_boundsl = [];

y_scanr = [];
lane_boundsr = [];

for n = 1:length(y_scan)
    if lane_bounds(n,1) > -999
        y_scanl = [y_scanl; y_scan(n)];
        lane_boundsl = [lane_boundsl; lane_bounds(n,1)];
    end
    if lane_bounds(n,2) > -999
        y_scanr = [y_scanr; y_scan(n)];
        lane_boundsr = [lane_boundsr; lane_bounds(n,1)];
    end
end

% [beta_l, mse_l, n_l] = adaptive_linefitting(y_scanl', lane_boundsl)

[beta_r, mse_r, n_r] = adaptive_linefitting(y_scanr', lane_boundsr)

figure;
subplot(2,1,1);
plot(y_scanl, lane_boundsl, 'ro'); hold on;
plot([y_scan(1) y_scan(end)], beta_l(2) + beta_l(1) .* [y_scan(1) y_scan(end)], 'b-'); grid on;

subplot(2,1,2);
plot(y_scanr, lane_boundsr, 'ro'); hold on;
plot([y_scan(1) y_scan(end)], beta_r(2) + beta_r(1) .* [y_scan(1) y_scan(end)], 'b-'); grid on;
% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

clc; clear all; close all;

filename = 'data/clip2/img185.jpg';
load_settings; % Load processing settings

frame = imread(filename);

result = adaptive_lanedetection(frame, settings);

% Make plots
figure;
imshow(frame); hold on;

y_scan = floor(0:settings.rect(4)/(settings.segments-1):settings.rect(4));
y_scan(1) = 1;
x_lcenter = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(1), settings.params(2)));
x_rcenter = settings.rect(3) - x_lcenter;
x_halfspan = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(3), settings.params(4)));

for k = 1:settings.segments
    l = settings.rect(1) + x_lcenter(k);
    r = settings.rect(1) + x_rcenter(k);
    w = x_halfspan(k);
    y = settings.rect(2) + y_scan(k);
    % plot([l-w, l+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
    % plot([r-w, r+w], [y y], 'LineWidth', 1, 'Color', 'yellow');

    if result.lane_bounds(k,1) > -900
        plot(l+result.lane_bounds(k,1), y, 'rx');
    end

    if result.lane_bounds(k,2) > -900
        plot(r+result.lane_bounds(k,2), y, 'rx');
    end
end

% Plot detected lane markers
    % Left lane
    if result.confidence.l > 0.55
        x1 = settings.rect(1) + settings.params(1) + result.beta.l(2);
        x2 = settings.rect(1) + settings.params(2) + result.beta.l(2) + result.beta.l(1) * settings.rect(4);
        plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'white');
    end
    % Right lane
    if result.confidence.r > 0.55
        x1 = settings.rect(1) + (settings.rect(3) - settings.params(1)) + result.beta.r(2);
        x2 = settings.rect(1) + (settings.rect(3) - settings.params(2)) + result.beta.r(2) + result.beta.r(1) * settings.rect(4);
        plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'white');
    end
    
    % Tracking lane
    if result.confidence.l > 0.5 || result.confidence.r > 0.5
        x1 = settings.rect(1) + settings.rect(3)/2 + result.beta.c(2);
        x2 = settings.rect(1) + settings.rect(3)/2 + result.beta.c(2) + result.beta.c(1) * settings.rect(4);
        plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'magenta');
    end

% Steering command
steer = [100 1] * result.beta.c;
plot([size(frame,2)/2 size(frame,2)/2+steer*3], [size(frame,1)-8, size(frame,1)-8], 'LineWidth', 10, 'Color', 'green');
plot([size(frame,2)/2, size(frame,2)/2], [size(frame,1)-20, size(frame,1)-1], 'LineWidth', 1, 'Color', 'yellow');

% Show lane confidences
text(10,16,[sprintf('%.2f',result.confidence.l*100), '%'],'Color','red');
text(10,32,[sprintf('%.2f',result.confidence.r*100), '%'],'Color','red');
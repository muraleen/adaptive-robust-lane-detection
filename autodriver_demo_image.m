% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

clc; clear all; close all;

filename = 'data/clip1/img87.jpg';
load_settings; % Load processing settings

frame = imread(filename);

result = autodriver_process_frame(frame, settings);

% Make plots
figure;
imshow(frame); hold on;

for k = 1:size(settings.centers_linesearch,1)
    y = settings.rect_linesearch(2) + settings.centers_linesearch(k,1);
    w = settings.centers_linesearch(k,2);
    l = settings.rect_linesearch(1) + settings.centers_linesearch(k,3);
    r = settings.rect_linesearch(1) + settings.centers_linesearch(k,4);
    plot([l-w, l+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
    plot([r-w, r+w], [y y], 'LineWidth', 1, 'Color', 'yellow');

    if result.lanes(k,1) ~= 1000
        plot(l+result.lanes(k,1), y, 'rx');
    end

    if result.lanes(k,2) ~= 1000
        plot(r+result.lanes(k,2), y, 'rx');
    end
end

plot([size(frame,2)/2 size(frame,2)/2+result.steer*3], [size(frame,1)-8, size(frame,1)-8], 'LineWidth', 10, 'Color', 'green');
plot([size(frame,2)/2, size(frame,2)/2], [size(frame,1)-20, size(frame,1)-1], 'LineWidth', 1, 'Color', 'yellow');

if result.quality == 1 || result.quality == 3
    plot(settings.rect_linesearch(1)+([1 1; 250 1] * result.plot.left_beta)', settings.rect_linesearch(2)+[1, 250], 'LineWidth', 2, 'Color', 'magenta');
end
if result.quality == 2 || result.quality == 3
    plot(settings.rect_linesearch(1)+([1 1; 250 1] * result.plot.right_beta)', settings.rect_linesearch(2)+[1, 250], 'LineWidth', 2, 'Color', 'magenta');
end

text(10,16,num2str(result.quality),'Color','red');
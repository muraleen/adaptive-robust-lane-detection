function [BW] = threshold_labspace(RGB, c1min, c1max)

    % Convert RGB image to chosen color space
    I = rgb2lab(RGB);

    % Define thresholds for channel 1 based on histogram settings
    channel1Min = c1min;
    channel1Max = c1max;

    % Define thresholds for channel 2 based on histogram settings
    channel2Min = -100;
    channel2Max = 100;

    % Define thresholds for channel 3 based on histogram settings
    channel3Min = -100;
    channel3Max = 100;

    % Create mask based on chosen histogram thresholds
    sliderBW = (I(:,:,1) >= channel1Min ) & (I(:,:,1) <= channel1Max) & ...
        (I(:,:,2) >= channel2Min ) & (I(:,:,2) <= channel2Max) & ...
        (I(:,:,3) >= channel3Min ) & (I(:,:,3) <= channel3Max);
    BW = sliderBW;

end

% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

clc; clear all; close all;

filename = 'data/clip2.avi'; % Filetype must be AVI, MJ2, OGG or OGV
target = 'out/clip3_3.avi';

frame_start = 62500;
frame_end = 92500;

load_settings; % Load processing settings
video = VideoReader(filename); % Load video from file

output = VideoWriter(target); % Output video target
output.FrameRate = settings.fps_process;
open(output);

disp(['Loaded video file: ' filename ' | duration: ' num2str(floor(video.Duration/60)) ' min ' num2str(round(mod(video.Duration,60))) ' sec | framerate: ' num2str(round(video.FrameRate)) ' fps']);

frame_id = 1;

results = [];

steer = 0;
track = [0;0];

while hasFrame(video)
    % Read frame from video
    frame = readFrame(video);
    
    if frame_id > frame_start
    
        % Process only at the fps_process
        if mod(frame_id, floor(ceil(video.FrameRate)/settings.fps_process)) == 0
            disp(frame_id);
            result = adaptive_lanedetection(frame, settings);
            results = [results; result];

            % Create frame for video file
            h = figure('Visible', 'off');
            a = axes('Visible', 'off');

            % Make plots
            imshow(frame); hold on;

            y_scan = floor(0:settings.rect(4)/(settings.segments-1):settings.rect(4));
            y_scan(1) = 1;
            x_lcenter = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(1), settings.params(2)));
            x_rcenter = settings.rect(3) - x_lcenter;
            x_halfspan = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(3), settings.params(4)));

            for k = 1:settings.segments
                l = settings.rect(1) + x_lcenter(k);
                r = settings.rect(1) + x_rcenter(k);
                w = x_halfspan(k);
                y = settings.rect(2) + y_scan(k);
                % plot([l-w, l+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
                % plot([r-w, r+w], [y y], 'LineWidth', 1, 'Color', 'yellow');

                if result.lane_bounds(k,1) > -900
                    plot(l+result.lane_bounds(k,1), y, 'rx');
                end

                if result.lane_bounds(k,2) > -900
                    plot(r+result.lane_bounds(k,2), y, 'rx');
                end
            end

            % Plot detected lane markers
            % Left lane
            if result.confidence.l > 0.55
                x1 = settings.rect(1) + settings.params(1) + result.beta.l(2);
                x2 = settings.rect(1) + settings.params(2) + result.beta.l(2) + result.beta.l(1) * settings.rect(4);
                plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 1, 'Color', 'magenta');
            end
            % Right lane
            if result.confidence.r > 0.55
                x1 = settings.rect(1) + (settings.rect(3) - settings.params(1)) + result.beta.r(2);
                x2 = settings.rect(1) + (settings.rect(3) - settings.params(2)) + result.beta.r(2) + result.beta.r(1) * settings.rect(4);
                plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 1, 'Color', 'magenta');
            end
            
            % Steering command
            tmp = saturate([100 1] * result.beta.c,-150,150);
            tmp2 = saturate(result.beta.c, [-1; -200], [1; 200]);
            conf = saturate((result.confidence.l + result.confidence.r)/2, 0, 1);
            if abs(tmp < 150) && conf > 0.3 && conf < 1
                steer = saturate((1 - conf/10) * steer + conf/10 * tmp,-150,150); % Fading filter based on confidence
                track = (1 - conf/10) .* track + conf/10 .* tmp2;
            else
                steer = saturate(0.9 * steer, -150, 150);
                track = 0.9 .* track;
            end
            
            % Tracking lane
            x1 = settings.rect(1) + settings.rect(3)/2 + track(2);
            x2 = settings.rect(1) + settings.rect(3)/2 + track(2) + track(1) * settings.rect(4);
            plot([x1 x2], settings.rect(2) + [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'white');
            
            plot([size(frame,2)/2 size(frame,2)/2+tmp*2], [size(frame,1)-10, size(frame,1)-10], 'LineWidth', 6, 'Color', 'yellow');
            plot([size(frame,2)/2 size(frame,2)/2+steer*2], [size(frame,1)-8, size(frame,1)-8], 'LineWidth', 10, 'Color', 'green');
            plot([size(frame,2)/2, size(frame,2)/2], [size(frame,1)-20, size(frame,1)-1], 'LineWidth', 1, 'Color', 'yellow');

            % Show lane confidences
            text(10,16,[sprintf('%.2f',result.confidence.l*100), '%'],'Color','red');
            text(10,32,[sprintf('%.2f',result.confidence.r*100), '%'],'Color','red');

            image = getframe(h);
            writeVideo(output, image);
            
        end
        
    end
    
    if frame_id > frame_end
        break;
    end
    
    frame_id = frame_id + 1;
end

close(output);

% save('out/clip2_results.mat', 'results');
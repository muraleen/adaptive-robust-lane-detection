function [ out ] = pool( image, p )
    h = size(image,1);
    w = size(image,2);
    out = zeros(ceil(h/p), ceil(w/p));
    im2 = zeros(p*ceil(h/p), p*ceil(w/p));
    im2(1:h,1:w) = image;
    for y = 1:ceil(h/p)
        for x = 1:ceil(w/p)
            out(y,x) = max(max(im2((y-1)*p+1:y*p, (x-1)*p+1:x*p)));
        end
    end
end


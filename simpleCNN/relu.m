function [ out ] = relu( image )
    out = image;
    out(out<0) = 0;
end


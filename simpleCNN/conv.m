function [ out ] = conv( image, feature )
    out = conv2(image, feature) ./ (length(feature)^2);
end


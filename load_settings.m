% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

settings.fps_process = 15; % Effective processing fps

% SETTINGS FOR 720P SONY ACTION CAMERA
settings.rect = [120 200 1040 300]; % Line search region (x,y,w,h)
settings.segments = 20;
settings.params = [470, 200, 40, 180]; % [top_left_lane, bottom_left_lane, top_search_width, bottom_search_width]
settings.gaussfilt_sigma = 2;

% SETTINGS FOR 1080P GALAXY S8 CAMERA
% settings.rect = [20 500 1880 350]; % Line search region (x,y,w,h)
% settings.segments = 20;
% settings.params = [780, 310, 150, 300]; % [top_left_lane, bottom_left_lane, top_search_width, bottom_search_width]
% settings.gaussfilt_sigma = 2;
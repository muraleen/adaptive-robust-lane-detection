% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

function [ result ] = adaptive_lanedetection( frame, settings )
    
    devplot = 0; % Set to 1 to plot while developing/debugging
    error_score = 100; % Score to assign if there is an error
    
    % Crop image to search frame region
    img = imcrop(frame, settings.rect);
    % Blur image to remove noise
    img = imgaussfilt(img, settings.gaussfilt_sigma);
    
    % Process: Run the lane detection algorithm with various thresholds
    % and select the data with the best quality score. Assume lanes are
    % straight lines - use adaptive_regression to select which data points
    % to use for the final lanes
    
    result.score = 9999;
    result.score_l = 9999;
    result.score_r = 9999;
    result.lane_bounds = zeros(settings.segments,2);
    result.beta.l = [0;0];
    result.beta.r = [0;0];
    result.plot = [];
    result.lab_c1range = [];
    
    % Test different L*a*b space Channel 1 thresholds
    for c1min = 80:-10:30
        lane_bounds = zeros(settings.segments,2)-999; % Placeholder for tmp bounds
        
        % STEP 1: Thrshold image using L*a*b channel 1 = c1min -> c1min+20
        bw = threshold_labspace(img, c1min, c1min+20);
        
        % STEP 2: Find lane bounds for each segment
        % Compute y steps to scane, lane centers and search widths
        y_scan = floor(0:settings.rect(4)/(settings.segments-1):settings.rect(4));
        y_scan(1) = 1;
        x_lcenter = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(1), settings.params(2)));
        x_rcenter = settings.rect(3) - x_lcenter;
        x_halfspan = floor(interpolate2d(y_scan, 1, settings.rect(4), settings.params(3), settings.params(4)));
        
        % For each segment
        for n = 1:settings.segments
            % Start from the inside and scan outwards.
            for scan = -x_halfspan(n):x_halfspan(n)
                % Left Lane
                if lane_bounds(n,1) == -999
                    if bw(y_scan(n), x_lcenter(n)-scan+1) == 0 && bw(y_scan(n), x_lcenter(n)-scan) == 1
                        lane_bounds(n,1) = -scan;
                    end
                end
                
                % Right Lane
                if lane_bounds(n,2) == -999
                    if bw(y_scan(n), x_rcenter(n)+scan-1) == 0 && bw(y_scan(n), x_rcenter(n)+scan) == 1
                        lane_bounds(n,2) = scan;
                    end
                end
                
                % Break out if lane bounds have already been found
                if lane_bounds(n,1) > -999 && lane_bounds(n,2) > -999
                    break;
                end
            end
        end
        
        % Remove empty lane_bound entries
        y_scanl = [];
        lane_boundsl = [];
        y_scanr = [];
        lane_boundsr = [];

        for n = 1:length(y_scan)
            if lane_bounds(n,1) > -900
                y_scanl = [y_scanl; y_scan(n)];
                lane_boundsl = [lane_boundsl; lane_bounds(n,1)];
            end
            if lane_bounds(n,2) > -900
                y_scanr = [y_scanr; y_scan(n)];
                lane_boundsr = [lane_boundsr; lane_bounds(n,2)];
            end
        end

        % STEP 3: Perform adaptive line fitting on data
        [beta_l, score_l, ~] = adaptive_linefitting(y_scanl', lane_boundsl);
        [beta_r, score_r, ~] = adaptive_linefitting(y_scanr', lane_boundsr);
        
        if size(beta_l,1) == 0
            beta_l = [0; 0];
        end
        
        if size(beta_r,1) == 0
            beta_r = [0; 0];
        end
        
%         % Convert beta values to be able to plot
%         delta = (settings.params(2) - settings.params(1))/2;
%         beta_p = 2 * delta / (1 - settings.rect(4));
%         slope_l = interpolate2d(beta_l(1), 0, beta_p, delta, 0);
%         slope_r = interpolate2d(beta_r(1), 0, beta_p, delta, 0);
        
        % Limit score to 1000
        if score_l > error_score
            score_l = error_score;
        end
        if score_r > error_score
            score_r = error_score;
        end

        % Plot lines onto image
        if devplot
            figure;
            imshow(bw); hold on;
            
            % Plot all detected lane bounds
            for l = 1:settings.segments
                % Left lane
                if lane_bounds(l,1) > -900
                    plot(x_lcenter(l)+lane_bounds(l,1), y_scan(l), 'rx');
                end
                % Right lane
                if lane_bounds(l,2) > -900
                    plot(x_rcenter(l)+lane_bounds(l,2), y_scan(l), 'rx');
                end
            end
            
            % Plot detected lane markers
            % Left lane
            if score_l < 50
                x1 = settings.params(1) + beta_l(2);
                x2 = settings.params(2) + beta_l(2) + beta_l(1) * settings.rect(4);
                plot([x1 x2], [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'magenta');
            end
            % Right lane
            if score_r < 50
                x1 = (settings.rect(3) - settings.params(1)) + beta_r(2);
                x2 = (settings.rect(3) - settings.params(2)) + beta_r(2) + beta_r(1) * settings.rect(4);
                plot([x1 x2], [1 settings.rect(4)], 'LineWidth', 2, 'Color', 'magenta');
            end
        end
        
        if score_l + score_r < result.score
            result.score_l = score_l;
            result.score_r = score_r;
            result.score = score_l + score_r;
            result.lane_bounds = lane_bounds;
            result.beta.l = beta_l;
            result.beta.r = beta_r;
            result.confidence.l = 0;
            result.confidence.r = 0;
            if score_l < 80 && abs(beta_l(1)) < 1.5 && abs(beta_l(2)) < 300
                result.plot.lanes.l.x1 = settings.params(1) + beta_l(2);
                result.plot.lanes.l.x2 = settings.params(2) + beta_l(2) + beta_l(1) * settings.rect(4);
                result.confidence.l = saturate(1 - score_l/error_score, 0, 1);
            end
            if score_r < 80 && abs(beta_r(1)) < 1.5 && abs(beta_r(2)) < 300
                result.plot.lanes.r.x1 = (settings.rect(3) - settings.params(1)) + beta_r(2);
                result.plot.lanes.r.x2 = (settings.rect(3) - settings.params(2)) + beta_r(2) + beta_r(1) * settings.rect(4);
                result.confidence.r = saturate(1 - score_r/error_score, 0, 1);
            end
            result.lab_c1range = [c1min, c1min+20];
            
            % Find center line to track
            total_confidence = result.confidence.l + result.confidence.r;
            result.beta.c = (beta_l .* result.confidence.l + beta_r .* result.confidence.r) / total_confidence;
        end
    end

end
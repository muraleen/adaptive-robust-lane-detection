% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

clc; clear all; close all;

filename = 'data/clip2.avi'; % Filetype must be AVI, MJ2, OGG or OGV
target = 'out/clip2_2v2.avi';

frame_start = 40001;
frame_end = 60000;

load_settings; % Load processing settings
video = VideoReader(filename); % Load video from file

output = VideoWriter(target); % Output video target
output.FrameRate = settings.fps_process;
open(output);

disp(['Loaded video file: ' filename ' | duration: ' num2str(floor(video.Duration/60)) ' min ' num2str(round(mod(video.Duration,60))) ' sec | framerate: ' num2str(round(video.FrameRate)) ' fps']);

frame_id = 1;

results = [];

while hasFrame(video)
    % Read frame from video
    frame = readFrame(video);
    
    if frame_id > frame_start
    
        % Process only at the fps_process
        if mod(frame_id, settings.fps_process) == 0
            disp(frame_id);
            result = autodriver_process_frame(frame, settings);
            results = [results; result];

            % Create frame for video file
            h = figure('Visible', 'off');
            a = axes('Visible', 'off');

            % Make plots
            imshow(frame); hold on;

           for k = 1:size(settings.centers_linesearch,1)
                y = settings.rect_linesearch(2) + settings.centers_linesearch(k,1);
                w = settings.centers_linesearch(k,2);
                l = settings.rect_linesearch(1) + settings.centers_linesearch(k,3);
                r = settings.rect_linesearch(1) + settings.centers_linesearch(k,4);
                plot([l-w, l+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
                plot([r-w, r+w], [y y], 'LineWidth', 1, 'Color', 'yellow');

                if result.lanes(k,1) ~= 1000
                    plot(l+result.lanes(k,1), y, 'rx');
                end

                if result.lanes(k,2) ~= 1000
                    plot(r+result.lanes(k,2), y, 'rx');
                end
            end

            plot([size(frame,2)/2 size(frame,2)/2+result.steer*3], [size(frame,1)-8, size(frame,1)-8], 'LineWidth', 10, 'Color', 'green');
            plot([size(frame,2)/2, size(frame,2)/2], [size(frame,1)-20, size(frame,1)-1], 'LineWidth', 1, 'Color', 'magenta');

            if result.quality == 1 || result.quality == 3
                plot(settings.rect_linesearch(1)+([1 1; 250 1] * result.plot.left_beta)', settings.rect_linesearch(2)+[1, 250], 'LineWidth', 2, 'Color', 'magenta');
            end
            if result.quality == 2 || result.quality == 3
                plot(settings.rect_linesearch(1)+([1 1; 250 1] * result.plot.right_beta)', settings.rect_linesearch(2)+[1, 250], 'LineWidth', 2, 'Color', 'magenta');
            end
            
            text(10,16,num2str(result.quality),'Color','red');

            image = getframe(h);
            % imwrite(image.cdata, 'out/img.jpg');
            writeVideo(output, image);
            
        end
        
    end
    
    if frame_id > frame_end
        break;
    end
    
    frame_id = frame_id + 1;
end

close(output);

% save('clip1_results.mat', 'results');
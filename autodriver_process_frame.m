% Project: AutoDriver
% Version: v0.1
% Copyright (c) Aptus Aerospace, LLC. 2017
% Permission is hereby granted, free of charge, to any person obtaining a
% copy of this software and associated documentation files (the
% "Software"), to deal in the Software without restriction, including
% without limitation the rights to use, copy, modify, merge, publish,
% distribute, sublicense, and/or sell copies of the Software, and to permit
% persons to whom the Software is furnished to do so, subject to the
% following conditions:
% 
% The above copyright notice and this permission notice shall be included
% in all copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
% OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
% MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
% NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
% DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
% OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
% USE OR OTHER DEALINGS IN THE SOFTWARE.

function [ result ] = autodriver_process_frame( frame, settings )
    
    devplot = 0;

    % Run Sobel edge detection algorithm to find lane edges
    region_linesearch = imcrop(frame, settings.rect_linesearch);
    [thresh_linesearch,~] = autodriver_createMask(region_linesearch);
    % filter_linesearch = imgaussfilt(rgb2gray(region_linesearch), 2);
    % thresh_linesearch = imbinarize(filter_linesearch, 0.68);

    target = thresh_linesearch;

    % Plot
    if devplot
        imshow(target); hold on;
    end

    lane_center = zeros(size(settings.centers_linesearch,1),2); % Lane centers
    
    left_lanebound = [];
    right_lanebound = [];
    
    for k = 1:size(settings.centers_linesearch,1)
        y = settings.centers_linesearch(k,1);
        w = settings.centers_linesearch(k,2);
        l = settings.centers_linesearch(k,3);
        r = settings.centers_linesearch(k,4);

        % Plot
        if devplot
            plot([l-w, l+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
            plot([r-w, r+w], [y y], 'LineWidth', 1, 'Color', 'yellow');
        end

        % Search for the first intersection from inside
        left_start = 1000;
        right_start = 1000;

        for x = -w:w
            % Left lane
            if left_start == 1000 && target(y,l-x) == 1
                left_start = -x;
            end

            % Right lane
            if right_start == 1000 && target(y,r+x) == 1
                right_start = x;
            end
            
            % Edges
            lane_center(k,:) = [left_start, right_start];
        end
        
        if lane_center(k,1) ~= 1000
            left_lanebound = [left_lanebound; y, lane_center(k,1), l+lane_center(k,1)];
        end
        
        if lane_center(k,2) ~= 1000
            right_lanebound = [right_lanebound; y, lane_center(k,2), r+lane_center(k,2)];
        end

        % Plot
        if devplot
            if lane_center(k,1) ~= 1000
                plot([l+lane_center(k,1), l+lane_center(k,1)], [y-5, y+5], 'LineWidth', 2, 'Color', 'red');
            end

            if lane_center(k,2) ~= 1000
                plot([r+lane_center(k,2), r+lane_center(k,2)], [y-5, y+5], 'LineWidth', 2, 'Color', 'red');
            end
        end

%         if lane_center(k,1) ~= 1000 && lane_center(k,2) ~= 1000
%             good_data = good_data + 1;
%             steer = ((good_data-1) * steer + (lane_center(k,1) + lane_center(k,2))/2) / good_data;
%         end

    end
    
    % Fit lines
    leftY = [];
    rightY = [];
    leftYP = [];
    rightYP = [];
    leftX = [];
    rightX = [];
    
    leftN = size(left_lanebound,1);
    rightN = size(right_lanebound,1);
    
    for l = 1:leftN
        leftX = [leftX; left_lanebound(l,1) 1];
        leftY = [leftY; left_lanebound(l,2)];
        leftYP = [leftYP; left_lanebound(l,3)];
    end
    for l = 1:rightN
        rightX = [rightX; right_lanebound(l,1) 1];
        rightY = [rightY; right_lanebound(l,2)];
        rightYP = [rightYP; right_lanebound(l,3)];
    end
    
    leftBeta = 0;
    rightBeta = 0;
    leftBetaP = 0;
    rightBetaP = 0;
    
    if leftN > 1
        leftBeta = leftX \ leftY;
        leftBetaP = leftX \ leftYP;
    end
    if rightN > 1
        rightBeta = rightX \ rightY;
        rightBetaP = rightX \ rightYP;
    end
    
    result.left_n = leftN;
    result.right_n = rightN;
    result.plot.left_beta = leftBetaP;
    result.plot.right_beta = rightBetaP;
    result.left_beta = leftBeta;
    result.right_beta = rightBeta;
    result.quality = 0;
    result.steer = 0;
    result.left_mse = 0;
    result.right_mse = 0;
    
    if leftN > 1
        for l = 1:leftN
            result.left_mse = result.left_mse + (leftX(l,:) * leftBeta - left_lanebound(l,2))^2;
        end
    end
    
    if rightN > 1
        for l = 1:rightN
            result.right_mse = result.right_mse + (rightX(l,:) * rightBeta - right_lanebound(l,2))^2;
        end
    end
    
    left_ok = 0;
    right_ok = 0;
    
    if leftN > 1
        result.left_mse = result.left_mse / leftN;
    end
    
    if rightN > 1
        result.right_mse = result.right_mse / rightN;
    end
    
    if leftN > 1 && abs(leftBeta(1)) < 0.8 && abs(leftBeta(2)) < 150 && result.left_mse < 80
        left_steer = [150 1] * leftBeta;
        result.steer = left_steer;
        result.quality = 1;
        left_ok = 1;
    end
    
    if rightN > 1 && abs(rightBeta(1)) < 0.8 && abs(rightBeta(2)) < 150 && result.right_mse < 80
        right_steer = [150 1] * rightBeta;
        result.steer = right_steer;
        right_ok = 1;
        result.quality = 2;
    end
    
    % Compute steer demand
    if left_ok && right_ok
        result.steer = (left_steer + right_steer) / 2;
        result.quality = 3;
    end
    
    % Plot
    if devplot
        plot(([1 1; 250 1] * leftBetaP)', [1, 250], 'LineWidth', 2, 'Color', 'magenta');
        plot(([1 1; 250 1] * rightBetaP)', [1, 250], 'LineWidth', 2, 'Color', 'magenta');
        plot([520 520+result.steer*2], [244, 244], 'LineWidth', 10, 'Color', 'green');
        plot([520, 520], [230, 250], 'LineWidth', 1, 'Color', 'yellow');
        text(10,10,num2str(result.quality),'Color','red');
    end
    
    result.lanes = lane_center;

end

